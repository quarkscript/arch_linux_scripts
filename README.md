## Arch Linux Scripts
My shell scripts writed on Arch Linux for my Arch Linux

#### [blist](blist/blist) or [blist_l](blist/blist_l)
The last step in my implementing batch installation scripts for Arch Linux. It uses a pseudo-graphic interface and makes it easy to partition and format disks.
There are some video demos: [uefi](https://www.youtube.com/watch?v=tlfbd5tCG6Y), [bios](https://www.youtube.com/watch?v=EZk5_hIgQIg), [uefi + nvme + qemu](https://tube.tchncs.de/w/ambWmke7uRzsJri9qTdCV5). There is a version with Ukrainian localization - [blist_l](blist/blist_l) (should be functional, but little bit outdated, update maybe appear in future)

**Встановлювач Арч Лінукса на українській мові - [blist_l](blist/blist_l)** ([demo](https://www.youtube.com/watch?v=PPibKEx33vM))

#### [example of config](archive/some_conf)

#### [linux-localgen](linux-localgen)
Arch linux kernel build script that tries to tune kernel config to local cpu.

#### [some-scripts](some-scripts)
- [sfslib](some-scripts/sfslib) - small lib of shell scrits used on my projects. It could be used as standalone script, for example to install package from AUR: `./sfslib aurget package-name` or `./sfslib armget aur-pkg-name` to install some (compatible but not all possible) AUR package on [archlinux|ARM](https://archlinuxarm.org)
- [runenv](some-scripts/runenv) - run virtual build environment on overlayed chrooted host system. I use it when want to rebuild linux kernel without installing build deps to host system.
- [c-cxx_tsts](some-scripts/c-cxx_tsts) - that was curious experiment to collect most influenced c/cxx flags to execution speed of gzip compressor (for example). Some lines may need to be uncommented.
- [fardu](some-scripts/fardu)
Simple shell script to find and remove duplicate files. It calcs checksums and compares it. Probably will work on any Linux.
- [color_sw](some-scripts/color_sw) scrit for modding default kde svg icons into [this](https://store.kde.org/p/1646781)

Scripts to make some custom confiruration on installed Arch Linux

- [uci](some-scripts/uci) -
install or remove a system service that reassigns the interrupt mapping of all USB controllers to the first processor core. It may be useful when copying to pen-drive is very slow.
- [pimi](some-scripts/pimi) -
ping mirrors and rearrange the pacman's mirrorlist depending of that pings

#### Archived projects, leaved as examples

- [batch-install-scripts](archive/batch-install-scripts)
Scripts make Arch Linux installation or cloning a bit easier. There is a [video demonstration](https://youtu.be/RuLSZkWwlC4). Disk partitioning isn't a part of these scripts, so you need to do it manually. The [getget](archive/batch-install-scripts/getget) script contains most of the settings, so edit it to change the settings as you like.
To make a list of installed packages from existed Arch Linux run from shell:
`pacman -Qqe >packages.txt` Since this project is archived, I suggest to use [blist](blist) instead.
- [pkgs_builds_scripts](archive/pkgs_builds_scripts)
In most case there are semi-automatic scripts, to build a custom binary based on AUR, compatible with Arch Linux package-system.
  - [ffmpeg-full](archive/pkgs_builds_scripts/get_ffmpeg-full_arch) - Used by ocheb scripts but it may be used as standalone script if you want integrate ffmpeg-full into your system. By the way if some of used PKGBUILDs from AUR required update this script may fail. In that case try to run this script later, after updates.
  - [ffmpeg_full_ocheb](archive/pkgs_builds_scripts/ffmpeg_full_ocheb) - build ffmpeg-full from AUR in overlay chrooted environment. [Demonstartion](https://youtu.be/CYMWXSBstpI)
  - [ffmpeg_full_amd_ocheb](archive/pkgs_builds_scripts/ffmpeg_full_amd_ocheb) - Same as previous, but for build another ffmpeg-full variant
  - [vlc-git](archive/pkgs_builds_scripts/get_last_vlc_arch)

>**For more help see scripts code**

#### [foo2zjs-arch](archive/foo2zjs-arch)
This is not my project, but since i steel use old cheap printer i decided to save somewhere worked driver with cups (for Arch Linux). Maybe it will be useful to someone.
