#!/bin/bash
# install awesome-git +yoru (git) on arch linux
# any build dependences will not affect the system
# run it from logged regular user

who=$(whoami)
if [ "$who" == "root" ]; then echo "
to run this script you must be a regular user who can run sudo, but not a root
" ; exit 1 ; fi
 
if !(cat /proc/filesystems | grep -q overlay ); then 
    echo !!! superuser password is required for modprobe overlay 
    sudo modprobe overlay
fi
if !(cat /proc/filesystems | grep -q overlay ); then
    echo "
    Seems like current linux kernel doesn't support overlay filesystem,
    overlayfs is required, next actions impossible without it.
    "; exit 1
fi

udir=upper
wdir=work
prdir=procdir
srcrt=/

if [ ! -d $udir ]; then mkdir $udir ; fi
if [ ! -d $wdir ]; then mkdir $wdir ; fi
if [ ! -d $prdir ]; then mkdir $prdir ; fi
if [ ! -d "$udir/build" ]; then 
    mkdir $udir/build
    sudo chown -R "$who" "$udir/build"
fi
if [ ! -d "$udir/etc" ]; then sudo mkdir $udir/etc ; fi
if [ ! -f "$udir/etc/resolv.conf" ]; then sudo cp -H /etc/resolv.conf $udir/etc/resolv.conf ;fi
    
    cd $udir/build
    
    # install deps
    sudo pacman -S rlwrap dex xcb-util-errors librsvg  rofi acpi acpid acpi_call upower lxappearance-gtk3 jq inotify-tools polkit-gnome xdotool xclip gpick ffmpeg blueman redshift pipewire pipewire-alsa pipewire-pulse alsa-utils brightnessctl feh maim mpv mpd mpc python-mutagen ncmpcpp playerctl xorg lightdm lightdm-gtk-greeter light-locker lightdm-gtk-greeter-settings xorg-xdm xorg-xclipboard xorg-xcalc xf86-input-vmmouse xf86-input-synaptics xf86-input-evdev xorg-xmessage xterm --needed --noconfirm #wezterm neovim firefox nautilus xfce4-power-manager    --needed --noconfirm
        
    echo '#!/bin/bash
# instlall build deps
pacman -S git fakeroot gcc cmake make pkg-config autoconf automake wget fontconfig libev luacheck --needed --noconfirm

# download shell (aur) helper script
sudo -u '$who' wget https://gitlab.com/quarkscript/arch_linux_scripts/-/raw/master/some-scripts/sfslib
chmod +x sfslib

# patch sfslib to install deps without questions
sed "s/pacman -S /pacman --noconfirm -S /g" -i sfslib
# patch sfslib to install builded packages without questions
sed "s/pacman -U /pacman --noconfirm -U /g" -i sfslib

chown -R '$who' ../build

# add nopass to sudoers to prevent password errors
echo "' $who' ALL=(ALL) NOPASSWD: ALL ">>/etc/sudoers

sudo -u '$who' ./sfslib aurget awesome-git
sudo -u '$who' ./sfslib aurget picom-git
#sudo -u '$who' ./sfslib aurget mpdris2

sudo -u '$who' git clone --depth 1 --recurse-submodules https://github.com/rxyhn/yoru.git

cd yoru && sudo -u '$who' git submodule update --remote --merge

' >runscr
    chmod +x runscr    
        
    cd ../../

sudo mount -t overlay overlay -o,lowerdir=$srcrt,upperdir=$udir,workdir=$wdir $prdir

sudo mount --bind /proc $prdir/proc
sudo mount --bind /dev $prdir/dev
sudo mount --bind /sys $prdir/sys
sudo mount --bind /tmp $prdir/tmp
    
sudo chroot $prdir /bin/bash <<EEOOF
cd /build
./runscr
exit
EEOOF
    
sudo umount $prdir/tmp
sudo umount $prdir/proc
sudo umount $prdir/dev
sudo umount $prdir/sys
sudo umount $prdir/

cd $udir
sudo mv var/cache/pacman/pkg/* /var/cache/pacman/pkg/
cd build
sudo pacman -U *.pkg.* --needed --noconfirm

#systemctl --user enable mpd.service
# systemctl --user start mpd.service

cd yoru
cp -fr config/* ~/.config/
cp -r misk/home/* ~/
sudo cp -r misc/fonts/* /usr/share/fonts
fc-cache -fv

#sudo systemctl start lightdm

echo "
if all complites succesfully you may remove '$udir' '$wdir' '$prdir'
and emable lightdm login manager like 'sudo systemctl enable lightdm'
or try it like 'sudo systemctl start lightdm'
"
